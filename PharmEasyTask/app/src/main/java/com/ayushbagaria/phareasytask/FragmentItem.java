package com.ayushbagaria.phareasytask;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.ayushbagaria.phareasytask.model.Drugs;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Objects;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FragmentItem extends Fragment {
    @Bind(R.id.img_drug_image)
    ImageView img_drug_image;
    @Bind(R.id.img_available)
    ImageView img_available;
    @Bind(R.id.tv_available)
    TextView tv_available;
    @Bind(R.id.tv_discount)
    TextView tv_discount;
    @Bind(R.id.tv_form)
    TextView tv_form;
    @Bind(R.id.tv_pack_form)
    TextView tv_pack_form;
    @Bind(R.id.tv_pack_size)
    TextView tv_pack_size;
    @Bind(R.id.tv_manufacture)
    TextView tv_manufacture;
    @Bind(R.id.tv_drug_label)
    TextView tv_drug_label;

    private  Drugs drug;
    private String form;
    private String label;
    private String packform;
    private String manufacture;
    private String packSize;
    private int discountPerc;
    private boolean availability;
    private Context context;
    private  int position;
    private boolean fragmentResume=false;
    private boolean fragmentVisible=false;
    private boolean fragmentOnCreated=false;

    public static FragmentItem newInstance( int position) {
        FragmentItem pageView= new FragmentItem();
        Bundle args = new Bundle();
        args.putInt("position", position);
        pageView.setArguments(args);
        return pageView;
    }

    @Override
    public  void onAttach(Activity activity){
        super.onAttach(activity);
        context = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.position = getArguments().getInt("position");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_item, container,
                false);
        ButterKnife.bind(this, layoutView);
        List<Drugs> drug = new Select().from(Drugs.class).where("id =?",position+1).execute();
        tv_available.setText(drug.get(0).getAvailable() ? R.string.available : R.string.notAvailable);
        img_available.setImageResource(drug.get(0).getAvailable() ? R.drawable.sign : R.drawable.cancel);
        tv_drug_label.setText(drug.get(0).getLabel());
        tv_form.setText(drug.get(0).getForm());
        tv_pack_form.setText(drug.get(0).getPackForm());
        tv_pack_size.setText(drug.get(0).getPackSize());
        tv_discount.setText(Integer.toString(drug.get(0).getDiscountPerc()) + "%");
        tv_manufacture.setText(drug.get(0).getManufacturer());
        img_drug_image.setImageResource(R.drawable.pill);
        return layoutView;
    }
    
}


