package com.ayushbagaria.phareasytask.adapter;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;

import com.ayushbagaria.phareasytask.FragmentItem;
import com.ayushbagaria.phareasytask.model.Drugs;

import java.util.List;

public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
    private List<Drugs> drugs;
    private int count;

    public ScreenSlidePagerAdapter(FragmentManager fm, int count) {
        super(fm);
        this.count = count;
    }

    @Override
    public void destroyItem(View collection, int position, Object o) {
        View view  = (View) o;

    }

    @Override
    public Fragment getItem(int position) {

        return FragmentItem.newInstance(position);
    }

    @Override
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
