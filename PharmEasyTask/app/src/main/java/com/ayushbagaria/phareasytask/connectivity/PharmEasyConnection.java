package  com.ayushbagaria.phareasytask.connectivity;

import android.content.Context;
import android.util.Log;

import com.ayushbagaria.phareasytask.listener.ResultCallBack;
import com.ayushbagaria.phareasytask.model.Drugs;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.ResponseBody;

import java.lang.reflect.Type;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class PharmEasyConnection {

  private static final String LOG_TAG = PharmEasyConnection.class.getSimpleName();
  private com.ayushbagaria.phareasytask.connectivity.PharmEasyConnector retroFitConnector;


  public PharmEasyConnection(Context context) {
    retroFitConnector = new PharmEasyConnector();
  }

  public void getResponse(final ResultCallBack<List<Drugs>> resultCallBack) {
    PharmEasyService service = retroFitConnector.createService(PharmEasyService.class);
    Call<ResponseBody> request = service.getResponse();
    request.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
        if (response.isSuccess()) {
          String responseString = "";
          try {
            responseString = response.body().string();
          } catch (Exception e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));
          }
          JsonElement jelement = new JsonParser().parse(responseString);
          JsonObject jobject = jelement.getAsJsonObject();
          JsonArray jarray = jobject.getAsJsonArray("result");
          Type projectListType = new TypeToken<List<Drugs>>() {}.getType();
          Gson gson = new Gson();
          List<Drugs> drugs =  gson.fromJson(String.valueOf(jarray),projectListType);
          resultCallBack.onResultCallBack(drugs, null);
        } else {
          resultCallBack.onResultCallBack(null, new Exception());
        }
      }

      @Override
      public void onFailure(Throwable t) {
        t.printStackTrace(System.err);
        resultCallBack.onResultCallBack(null, new Exception());
      }
    });
  }
}