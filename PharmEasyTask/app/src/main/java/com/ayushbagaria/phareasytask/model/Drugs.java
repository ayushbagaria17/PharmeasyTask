package com.ayushbagaria.phareasytask.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

@Table(name = "Drugs")
public class Drugs extends Model {

    @Expose
    @Column(name = "hkpDrugCode")
    public String hkpDrugCode;

    @Expose
    @Column(name = "generics")
    public String generics;

    @Expose
    @Column(name = "type")
    public String type;

    @Expose
    @Column(name = "available")
    public Boolean available;

    @Expose
    @Column(name = "Drugid")
    public int Drugid;

    @Expose
    @Column(name = "name")
    public String name;

    @Expose
    @Column(name = "mfId")
    public String mfId;

    @Expose
    @Column(name = "productsForBrand")
    public String productsForBrand;

    @Expose
    @Column(name = "discountPerc")
    public int discountPerc;

    @Expose
    @Column(name = "uPrice")
    public float uPrice;

    @Expose
    @Column(name = "mrp")
    public float mrp;

    @Expose
    @Column(name = "packSize")
    public String packSize;

    @Expose
    @Column(name = "slug")
    public String slug;

    @Expose
    @Column(name = "imgUrl")
    public String imgUrl;



    @Expose
    @Column(name = "su")
    public int su;

    @Expose
    @Column(name = "oPrice")
    public float oPrice;

    @Expose
    @Column(name = "packForm")
    public String packForm;

    @Expose
    @Column(name = "label")
    public String label;

    @Expose
    @Column(name = "manufacturer")
    public String manufacturer;

    @Expose
    @Column(name = "form")
    public String form;

    @Expose
    @Column(name = "pForm")
    public String Tablet;

    @Expose
    @Column(name = "uip")
    public int uip;


    public String getGenerics() {
        return generics;
    }

    public void setGenerics(String generics) {
        this.generics = generics;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public int getDrugid() {
        return Drugid;
    }

    public void setDrugid(int drugid) {
        Drugid = drugid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMfId() {
        return mfId;
    }

    public void setMfId(String mfId) {
        this.mfId = mfId;
    }

    public String getProductsForBrand() {
        return productsForBrand;
    }

    public void setProductsForBrand(String productsForBrand) {
        this.productsForBrand = productsForBrand;
    }

    public int getDiscountPerc() {
        return discountPerc;
    }

    public void setDiscountPerc(int discountPerc) {
        this.discountPerc = discountPerc;
    }

    public float getuPrice() {
        return uPrice;
    }

    public void setuPrice(float uPrice) {
        this.uPrice = uPrice;
    }

    public float getMrp() {
        return mrp;
    }

    public void setMrp(float mrp) {
        this.mrp = mrp;
    }

    public String getPackSize() {
        return packSize;
    }

    public void setPackSize(String packSize) {
        this.packSize = packSize;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getHkpDrugCode() {
        return hkpDrugCode;
    }

    public void setHkpDrugCode(String hkpDrugCode) {
        this.hkpDrugCode = hkpDrugCode;
    }

    public int getSu() {
        return su;
    }

    public void setSu(int su) {
        this.su = su;
    }

    public float getoPrice() {
        return oPrice;
    }

    public void setoPrice(float oPrice) {
        this.oPrice = oPrice;
    }

    public String getPackForm() {
        return packForm;
    }

    public void setPackForm(String packForm) {
        this.packForm = packForm;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public String getTablet() {
        return Tablet;
    }

    public void setTablet(String tablet) {
        Tablet = tablet;
    }

    public int getUip() {
        return uip;
    }

    public void setUip(int uip) {
        this.uip = uip;
    }


    public Drugs()
    {
        super();
    }

}
