package com.ayushbagaria.phareasytask;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.activeandroid.query.Select;
import com.ayushbagaria.phareasytask.connectivity.PharmEasyConnection;
import com.ayushbagaria.phareasytask.listener.ResultCallBack;
import com.ayushbagaria.phareasytask.model.Drugs;

import java.util.List;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 2000;
    SharedPreferences sharedpreferences;
    PharmEasyConnection pharmEasyConnection;

    private ResultCallBack<List<Drugs>> pharmeasyresultcallback = new ResultCallBack<List<Drugs>>() {
        @Override
        public void onResultCallBack(List<Drugs> drugResponse, Exception e) {
            for(Drugs drug : drugResponse){
                drug.save();
            }
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putInt(String.valueOf(R.string.downloaded), 1);
            editor.commit();
            getTotalItemCount(0);
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharedpreferences = getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
        int defaultValue = sharedpreferences.getInt(String.valueOf(R.string.downloaded),0);
        if (defaultValue == 0) {
            pharmEasyConnection = new PharmEasyConnection(this);
            pharmEasyConnection.getResponse(pharmeasyresultcallback);
        } else  {
            getTotalItemCount(1);
        }
    }

    private void getTotalItemCount(int downloaded) {
        final int count = new Select().from(Drugs.class).execute().size();
        if (downloaded == 1) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    i.putExtra("itemCount", count);
                    startActivity(i);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        } else {
            Intent i = new Intent(SplashActivity.this, MainActivity.class);
            i.putExtra("itemCount", count);
            startActivity(i);
            finish();
        }
    }
}
