package com.ayushbagaria.phareasytask.listener;

public interface ResultCallBack<T> {
  void onResultCallBack(T object, Exception e);
}
