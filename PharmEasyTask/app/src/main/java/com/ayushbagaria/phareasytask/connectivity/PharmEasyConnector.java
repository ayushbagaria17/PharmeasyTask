package  com.ayushbagaria.phareasytask.connectivity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;


import java.util.concurrent.TimeUnit;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class PharmEasyConnector {
  private static final String BASE_URL = "https://www.1mg.com";
  private static final String LOG_TAG = PharmEasyConnector.class.getSimpleName();
  private OkHttpClient httpClient;
  private Retrofit.Builder builder;

  public PharmEasyConnector() {
    Gson gson = new GsonBuilder().create();
    httpClient = new OkHttpClient();
    httpClient.setReadTimeout(5, TimeUnit.MINUTES);
    builder = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create(gson));
  }

  public <S> S createService(Class<S> serviceClass) {
    Retrofit retrofit = builder.client(httpClient).build();
    return retrofit.create(serviceClass);
  }
}
