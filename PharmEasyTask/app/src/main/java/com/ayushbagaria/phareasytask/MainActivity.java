package com.ayushbagaria.phareasytask;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.ayushbagaria.phareasytask.adapter.ScreenSlidePagerAdapter;
import com.ayushbagaria.phareasytask.connectivity.PharmEasyConnection;

import com.ayushbagaria.phareasytask.listener.ResultCallBack;

import com.ayushbagaria.phareasytask.model.Drugs;

import java.nio.channels.SelectableChannel;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity{
    @Bind(R.id.pager)
    ViewPager pager;
    ScreenSlidePagerAdapter mPagerAdapter;
    int count =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Intent i = getIntent();
        count = i.getIntExtra("itemCount",0);
        createPager();
    }

    private void createPager () {
            mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(), count);
            pager.setAdapter(mPagerAdapter);
    }

}
